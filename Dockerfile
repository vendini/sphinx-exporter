FROM alpine:3.5

COPY sphinx-exporter /usr/local/bin/sphinx-exporter
COPY app-start /usr/local/bin/app-start

RUN chmod 755 /usr/local/bin/sphinx-exporter && \
    chmod 755 /usr/local/bin/app-start

EXPOSE 9247

ENV SPHINX_ADDRESS 127.0.0.1:9306
ENV TELEMETRY_ADDRESS :9247
ENV TELEMETRY_ENDPOINT /metrics

CMD ["/usr/local/bin/app-start"]