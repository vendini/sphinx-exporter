# Sphinx Exporter

A simple server that scrapes sphinx index stats for consumption by prometheus.

## Getting Started

To run it:

```bash
./sphinx_exporter [flags]
```

Help on flags:

```bash
./sphinx_exporter --help
```

## Building Sphinx Exporter

From the sphinx-exporter directory
```bash
go install
```

## Building The Docker

build-docker.sh takes will produce a default container sphinx-exporter:latest unless you provide an alternate name as the only parameter to the script.
```bash
build-docker.sh {optional: dockername/default: sphinx-exporter:latest}
```

## Running The Docker Image

The docker is configured with three environment variables corresponding to the similarily named command-line arguments.

ENV SPHINX_ADDRESS 127.0.0.1:9306

ENV TELEMETRY_ADDRESS :9247

ENV TELEMETRY_ENDPOINT /metrics
