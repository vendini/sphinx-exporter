#!/bin/bash

PWD=$(pwd)

# Choose a name for the docker container
if [ -n "$1" ] ; then
  DOCKER_NAME=$1
else
  DOCKER_NAME="sphinx-exporter:latest"
fi

# Remove any existing binary -f(force) to inhibit errors
rm -f sphinx-exporter

# Build sphinx-exporter binary
go get; export GOOS=linux; export GOARCH=amd64; go build

# Build sphinx-exporter docker
docker build -t $DOCKER_NAME .

# Cleanup the binary
rm sphinx-exporter
