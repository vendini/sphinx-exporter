package main

import (
  "database/sql"
  "flag"
  "net/http"
  "strconv"
  "strings"
  "sync"

  _ "github.com/go-sql-driver/mysql"
  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/common/log"
)

const (
	namespace = "sphinx" // For Prometheus metrics.
)

var (
	indexLabelNames = []string{"index"}
	serverLabelNames  = []string{"request"}
)

func newIndexMetric(metricName string, docString string, constLabels prometheus.Labels) *prometheus.GaugeVec {
	return prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   namespace,
			Name:        "index_" + metricName,
			Help:        docString,
			ConstLabels: constLabels,
		},
		indexLabelNames,
	)
}

func newServerMetric(metricName string, docString string, constLabels prometheus.Labels) *prometheus.GaugeVec {
	return prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace:   namespace,
			Name:        "server_" + metricName,
			Help:        docString,
			ConstLabels: constLabels,
		},
		serverLabelNames,
  )
}

func newServerCounter(metricName string, docString string, constLabels prometheus.Labels) *prometheus.CounterVec {
	return prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace:   namespace,
			Name:        "server_" + metricName,
			Help:        docString,
			ConstLabels: constLabels,
		},
		serverLabelNames,
  )
}

type metrics map[int]*prometheus.Gauge
type counters map[int]*prometheus.Counter

type Exporter struct {
	sphinxAddress   string
  sphinxIndexes   string

	mutex sync.RWMutex

  getDBConnection func() (*sql.DB, error)

	up                                             prometheus.Gauge
	totalScrapes, csvParseFailures                 prometheus.Counter
	indexMetrics, serverMetrics map[string]*prometheus.GaugeVec
  serverCounters map[string]*prometheus.CounterVec
}


func NewExporter(sphinxAddress string) (*Exporter, error) {
  var getDBConnection func() (*sql.DB, error)
  getDBConnection = func() (*sql.DB, error) {
    return sql.Open("mysql", "@tcp(" + sphinxAddress + ")/")
  }

  return &Exporter{
    sphinxAddress: sphinxAddress,

    getDBConnection: getDBConnection,

    up: prometheus.NewGauge(prometheus.GaugeOpts{
      Namespace: namespace,
      Name:      "up",
      Help:      "Was the last scrape of sphinx successful.",
    }),
    totalScrapes: prometheus.NewCounter(prometheus.CounterOpts{
      Namespace: namespace,
      Name:      "exporter_total_scrapes",
      Help:      "Current total sphinx scrapes.",
    }),
    indexMetrics: map[string]*prometheus.GaugeVec{
      "indexed_documents": newIndexMetric("indexed_documents", "Number of documents indexed", nil),
      "indexed_bytes": newIndexMetric("indexed_bytes","Indexed Bytes", nil),
      "field_tokens_title": newIndexMetric("field_tokens_title", "Sums of per-field length titles over the entire index", nil),
      "field_tokens_body": newIndexMetric("field_tokens_body", "Sums of per-field length bodies over the entire index", nil),
      "total_tokens": newIndexMetric("total_tokens", "Total tokens", nil),
      "ram_bytes": newIndexMetric("ram_bytes", "total size (in bytes) of the RAM-resident index portion", nil),
      "disk_bytes": newIndexMetric("disk_bytes", "total size (in bytes) of the disk index", nil),
      "mem_limit": newIndexMetric("mem_limit", "Memory limit", nil),
    },
    serverCounters: map[string]*prometheus.CounterVec{
      "uptime": newServerCounter("uptime", "Uptime in seconds", nil),
      "connections": newServerCounter("connections", "Total connections", nil),
      "queries": newServerCounter("queries", "Query count", nil),
      "query_reads": newServerCounter("query_reads", "Query reads", nil),
      "query_readkb": newServerCounter("query_readkb", "Query read KB", nil),
      "query_readtime": newServerCounter("query_readtime", "Query read time", nil),
      "index_count": newServerCounter("index_count", "Query read time", nil),
      "command": newServerCounter("command", "Command stats", nil),
    },
    serverMetrics: map[string]*prometheus.GaugeVec{
      "maxed_out": newServerMetric("maxed_out", "Maxed out", nil),
      "agent_connect": newServerMetric("agent_connect", "Agent connect", nil),
      "agent_retry": newServerMetric("agent_retry", "Agent retry", nil),
      "query_wall": newServerMetric("query_wall", "Query wall", nil),
      "query_cpu": newServerMetric("query_cpu", "Query CPU", nil),
      "dist_queries": newServerMetric("dist_queries", "Dist queries", nil),
      "dist_wall": newServerMetric("dist_wall", "Dist wall", nil),
      "dist_local": newServerMetric("dist_local", "Dist local", nil),
      "dist_wait": newServerMetric("dist_wait", "Dist wait", nil),
      "avg_query_wall": newServerMetric("avg_query_wall", "Average query wall", nil),
      "avg_query_cpu": newServerMetric("avg_query_cpu", "Average query CPU", nil),
      "avg_dist_wall": newServerMetric("avg_dist_wall", "Average dist wall", nil),
      "avg_dist_local": newServerMetric("avg_dist_local", "Average dist local", nil),
      "avg_dist_wait": newServerMetric("avg_dist_wait", "Average dist wait", nil),
      "avg_query_reads": newServerMetric("avg_query_reads", "Average query reads", nil),
      "avg_query_readkb": newServerMetric("avg_query_readkb", "Average query read KB", nil),
      "avg_query_readtime": newServerMetric("avg_query_readtime", "Average query read time", nil),
    },
	}, nil
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
  for _, m := range e.indexMetrics {
    m.Describe(ch)
  }
	for _, m := range e.serverMetrics {
		m.Describe(ch)
	}
  for _, m := range e.serverCounters {
    m.Describe(ch)
  }

	ch <- e.up.Desc()
	ch <- e.totalScrapes.Desc()
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	e.mutex.Lock() // To protect metrics from concurrent collects.
	defer e.mutex.Unlock()

	e.resetMetrics()
	e.scrape()

	ch <- e.up
	ch <- e.totalScrapes
	e.collectMetrics(ch)
}

func (e *Exporter) collectMetrics(metrics chan<- prometheus.Metric) {
  for _, m := range e.indexMetrics {
		m.Collect(metrics)
	}
  for _, m := range e.serverMetrics {
		m.Collect(metrics)
	}
  for _, m := range e.serverCounters {
    m.Collect(metrics)
  }
}

func (e *Exporter) resetMetrics() {
  for _, m := range e.indexMetrics {
    m.Reset()
  }
	for _, m := range e.serverMetrics {
		m.Reset()
	}
  for _, m := range e.serverCounters {
    m.Reset()
  }
}

func (e *Exporter) scrape() {
	e.totalScrapes.Inc()

  db, err := e.getDBConnection()
  if err != nil {
    log.Errorf(err.Error())
    return
  }
  err = db.Ping()
  if err != nil {
    log.Errorf("%s", err.Error())
    return
  }
  defer db.Close()

  sphinxStats, err := db.Query("SHOW STATUS")
  if err != nil {
    log.Errorf(err.Error())
    return
  }

  defer sphinxStats.Close()

  statName := ""
  statValue := ""

  for sphinxStats.Next() {
    err := sphinxStats.Scan(&statName, &statValue)
    if err != nil {
      log.Errorf(err.Error())
      return
    }

    if statValue == "OFF" {
      statValue = "0"
    }

    if err != nil {
      log.Errorf(err.Error())
      return
    }

    if strings.Contains(statName, "command_") {
      parts := strings.Split(statName, "_")
      e.exportCounter(e.serverCounters, parts[0], statValue, parts[1])
    }
    if _, ok := e.serverCounters[statName]; ok {
      e.exportCounter(e.serverCounters, statName, statValue, "");
    }
    if _, ok := e.serverMetrics[statName]; ok {
      e.exportMetric(e.serverMetrics, statName, statValue, "");
    }
  }

  totalIndexes := float64(0)

  indexes, err := db.Query("SHOW TABLES")
  if err != nil {
    log.Errorf(err.Error())
    return
  }
  defer indexes.Close()

  for indexes.Next() {
    indexName := ""
    indexType := ""

    // Get next index
    err := indexes.Scan(&indexName, &indexType)
    if err != nil {
      log.Errorf(err.Error())
      return
    }

    // Retrieve stats for `indexName`
    stats, err := db.Query("SHOW INDEX " + indexName + " STATUS")
    if err != nil {
      log.Errorf(err.Error())
      return
    }

    defer stats.Close()

    // Set gauge for each stat for `indexName`
    for stats.Next() {
      statName := ""
      statValue := ""

      // Fetch stat
      err := stats.Scan(&statName, &statValue)
      if err != nil {
        log.Errorf(err.Error())
        return
      }

      if _, ok := e.indexMetrics[statName]; ok {
        e.exportMetric(e.indexMetrics, statName, statValue, indexName);
      }
    }

    totalIndexes++
  }

  e.serverCounters["index_count"].WithLabelValues("").Add(totalIndexes)

  e.up.Set(1)
}

func (e *Exporter) exportCounter(metrics map[string]*prometheus.CounterVec, metricName string, metricValue string, labels ...string) {
  value, _ := strconv.ParseFloat(metricValue, 64)
  metrics[metricName].WithLabelValues(labels...).Add(value)
}

func (e *Exporter) exportMetric(metrics map[string]*prometheus.GaugeVec, metricName string, metricValue string, labels ...string) {
  value, _ := strconv.ParseFloat(metricValue, 64)
  metrics[metricName].WithLabelValues(labels...).Set(value)
}

func main() {
	var (
    sphinxAddress             = flag.String("sphinx.address", "[127.0.0.1]:9306", "Sphinx MySQL address.")
    listenAddress             = flag.String("telemetry.address", ":9247", "Address on which to expose metrics.")
    listenEndpoint            = flag.String("telemetry.endpoint", "/metrics", "Path under which to expose metrics.")
	)
	flag.Parse()

	log.Infoln("Starting sphinx exporter")
  log.Infoln("telemetry.endpoint exporter", *listenEndpoint)
  log.Infoln("sphinx.address exporter", *sphinxAddress)

	exporter, err := NewExporter(*sphinxAddress)
	if err != nil {
		log.Fatal(err.Error())
	}
  prometheus.MustRegister(exporter)

	log.Infoln("Listening on", *listenAddress)

  http.Handle(*listenEndpoint, prometheus.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>Sphinx Exporter</title></head>
             <body>
             <h1>Sphinx Exporter</h1>
             <p><a href='` + *listenEndpoint + `'>Metrics</a></p>
             </body>
             </html>`))
	})
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}
